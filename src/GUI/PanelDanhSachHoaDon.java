package gui;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;

import dao.HoaDonDAO;
import entity.HoaDon;

public class PanelDanhSachHoaDon extends javax.swing.JPanel {
    
	private static final long serialVersionUID = 1L;
	
	/** Creates new form PanelDanhSachHoaDon */
    public PanelDanhSachHoaDon() {
        initComponents();
    }

    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable(model = new DefaultTableModel(
        		new String [] {"Mã hóa đơn", "Mã nhân viên", "Mã khách hàng", "Ngày lập hóa đơn"},0));
        jLabel1 = new javax.swing.JLabel();

//        jTable1.setModel(new javax.swing.table.DefaultTableModel(
//            new Object [][] {},
//            new String [] {
//                "Mã hóa đơn", "Mã nhân viên", "Mã khách hàng", "Ngày lập"
//            }
//        ) {
//            Class[] types = new Class [] {
//                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
//            };
//            boolean[] canEdit = new boolean [] {
//                false, false, false, false
//            };
//
//            public Class getColumnClass(int columnIndex) {
//                return types [columnIndex];
//            }
//
//            public boolean isCellEditable(int rowIndex, int columnIndex) {
//                return canEdit [columnIndex];
//            }
//        });
        
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
        }

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); 
        jLabel1.setText("Danh sách hóa đơn");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 865, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(324, 324, 324)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        
        loadBillsData();
    }


    private void loadBillsData() {
    	try {
			ArrayList<HoaDon> bills = new HoaDonDAO().getBills();
			
			for(HoaDon bill : bills)
				model.addRow(new Object[] {
						bill.getMaHoaDon(), bill.getMaNhanVien().getMaNhanVien(), bill.getMaKhachHang().getMaKH(), bill.getNgayLapHD()
				});
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private DefaultTableModel model;
}