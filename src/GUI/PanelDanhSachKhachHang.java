/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import dao.KhachHangDAO;
import entity.KhachHang;

public class PanelDanhSachKhachHang extends javax.swing.JPanel {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Creates new form PanelDanhSachKhachHang
	 */
	public PanelDanhSachKhachHang() {
		initComponents();
	}

	private void initComponents() {

		jScrollPane1 = new javax.swing.JScrollPane();
		tableDanhSachKhachHang = new javax.swing.JTable(model = new DefaultTableModel(new String[] { "Mã khách hàng", "Tên khách hàng", "SDT", "Địa chỉ" },0));
		btnThem = new javax.swing.JButton();
		btnChinhSua = new javax.swing.JButton();
		jLabel1 = new javax.swing.JLabel();
		btnXoa = new javax.swing.JButton();

//		tableDanhSachKhachHang.setModel(model = new javax.swing.table.DefaultTableModel(new Object[][] {},
//				new String[] { "Mã khách hàng", "Tên khách hàng", "SDT", "Địa chỉ" }) {
//			Class[] types = new Class[] { java.lang.String.class, java.lang.String.class, java.lang.String.class,
//					java.lang.String.class };
//			boolean[] canEdit = new boolean[] { false, false, false, false };
//
//			public Class getColumnClass(int columnIndex) {
//				return types[columnIndex];
//			}
//
//			public boolean isCellEditable(int rowIndex, int columnIndex) {
//				return canEdit[columnIndex];
//			}
//		});
		
		jScrollPane1.setViewportView(tableDanhSachKhachHang);
		if (tableDanhSachKhachHang.getColumnModel().getColumnCount() > 0) {
			tableDanhSachKhachHang.getColumnModel().getColumn(0).setResizable(false);
			tableDanhSachKhachHang.getColumnModel().getColumn(1).setResizable(false);
			tableDanhSachKhachHang.getColumnModel().getColumn(2).setResizable(false);
			tableDanhSachKhachHang.getColumnModel().getColumn(3).setResizable(false);
		}

		btnThem.setText("Thêm");
		btnThem.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnThemActionPerformed(evt);
			}
		});

		btnChinhSua.setText("Chỉnh sửa");
		btnChinhSua.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnChinhSuaActionPerformed(evt);
			}
		});

		jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
		jLabel1.setText("Danh sách khách hàng");

		btnXoa.setText("Xóa");
		btnXoa.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnXoaActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(jScrollPane1)
				.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
						.addContainerGap(162, Short.MAX_VALUE)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
										layout.createSequentialGroup().addComponent(jLabel1).addGap(303, 303, 303))
								.addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
										layout.createSequentialGroup().addComponent(btnXoa).addGap(153, 153, 153)
												.addComponent(btnChinhSua).addGap(151, 151, 151).addComponent(btnThem)
												.addGap(180, 180, 180)))));
		layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup().addContainerGap().addComponent(jLabel1)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
						.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 270,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addGap(18, 18, 18)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
								.addComponent(btnThem).addComponent(btnChinhSua).addComponent(btnXoa))
						.addContainerGap()));

        loadCustomersData();
	}

	private void btnThemActionPerformed(java.awt.event.ActionEvent evt) {
		new FrameThemKhachHang().setVisible(true);
	}

	private void btnChinhSuaActionPerformed(java.awt.event.ActionEvent evt) {
	}

	private void btnXoaActionPerformed(java.awt.event.ActionEvent evt) {
		int confirm = JOptionPane.showConfirmDialog(null, "Bạn có chắc muốn xóa?", "Xóa thông tin khách hàng",
				JOptionPane.YES_NO_OPTION);

	}

	private void loadCustomersData() {
		try {
			ArrayList<KhachHang> customers = new KhachHangDAO().getCustomers();

			for (KhachHang cus : customers)
				model.addRow(new Object[] { cus.getMaKH(), cus.getHoTen(), cus.getSoDT(), cus.getDiaChi() });

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton btnChinhSua;
	private javax.swing.JButton btnThem;
	private javax.swing.JButton btnXoa;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTable tableDanhSachKhachHang;
	private DefaultTableModel model;
	// End of variables declaration//GEN-END:variables
}
