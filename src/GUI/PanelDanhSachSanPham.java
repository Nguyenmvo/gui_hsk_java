package gui;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import dao.LinhKienDAO;
import entity.LinhKien;

public class PanelDanhSachSanPham extends javax.swing.JPanel {
    
    /**
     * Creates new form PanelDanhSach
     */
    public PanelDanhSachSanPham() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableDanhSachSanPham = new javax.swing.JTable(model = new DefaultTableModel(
        		new String [] {"Mã sản phẩm", "Tên sản phẩm", "Loại sản phẩm", "Giá"}, 0));
        
        btnThem = new javax.swing.JButton();
        btnChinhSua = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        btnXoa = new javax.swing.JButton();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jScrollPane1.setFocusable(false);

//        tableDanhSachSanPham.setModel(new javax.swing.table.DefaultTableModel(
//            new Object [][] {
//                {null, null, null, null},
//                {null, null, null, null},
//                {null, null, null, null},
//                {null, null, null, null}
//            },
//            new String [] {
//                "Mã sản phẩm", "Tên sản phẩm", "Loại sản phẩm", "Giá"
//            }
//        ) {
//            Class[] types = new Class [] {
//                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Double.class
//            };
//            boolean[] canEdit = new boolean [] {
//                false, false, false, false
//            };
//
//            public Class getColumnClass(int columnIndex) {
//                return types [columnIndex];
//            }
//
//            public boolean isCellEditable(int rowIndex, int columnIndex) {
//                return canEdit [columnIndex];
//            }
//        });
        
        jScrollPane1.setViewportView(tableDanhSachSanPham);
        if (tableDanhSachSanPham.getColumnModel().getColumnCount() > 0) {
            tableDanhSachSanPham.getColumnModel().getColumn(0).setResizable(false);
            tableDanhSachSanPham.getColumnModel().getColumn(1).setResizable(false);
            tableDanhSachSanPham.getColumnModel().getColumn(2).setResizable(false);
            tableDanhSachSanPham.getColumnModel().getColumn(3).setResizable(false);
        }

        btnThem.setText("Thêm");
        btnThem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemActionPerformed(evt);
            }
        });

        btnChinhSua.setText("Chỉnh sửa");
        btnChinhSua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChinhSuaActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Danh sách sản phẩm");

        btnXoa.setText("Xóa");
        btnXoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXoaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnXoa)
                .addGap(122, 122, 122)
                .addComponent(btnChinhSua)
                .addGap(148, 148, 148)
                .addComponent(btnThem)
                .addGap(177, 177, 177))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 860, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(306, 306, 306)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(21, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnThem)
                    .addComponent(btnChinhSua)
                    .addComponent(btnXoa))
                .addContainerGap())
        );
        
        loadProductsData();
    }// </editor-fold>//GEN-END:initComponents

    private void btnThemActionPerformed(java.awt.event.ActionEvent evt) {
        new FrameThemSanPham().setVisible(true);
    }

    private void btnChinhSuaActionPerformed(java.awt.event.ActionEvent evt) {
    }//GEN-LAST:event_btnChinhSuaActionPerformed

    private void btnXoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXoaActionPerformed
    	int confirm = JOptionPane.showConfirmDialog(null, "Bạn có chắc muốn xóa?", "Xóa thông tin sản phẩm",
				JOptionPane.YES_NO_OPTION);
    }//GEN-LAST:event_btnXoaActionPerformed

    private void loadProductsData() {
    	try {
			ArrayList<LinhKien> comps = new LinhKienDAO().getComponents();
			
			for(LinhKien comp : comps)
				model.addRow(new Object[] {
						comp.getMaLK(), comp.getTenLk(), comp.getLoaiLK(), comp.getDonGia()});
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnChinhSua;
    private javax.swing.JButton btnThem;
    private javax.swing.JButton btnXoa;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableDanhSachSanPham;
    private DefaultTableModel model;
    // End of variables declaration//GEN-END:variables
}
